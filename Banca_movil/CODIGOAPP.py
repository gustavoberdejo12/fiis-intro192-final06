#!/usr/bin/env python
# -*- coding: utf-8 -*-
from tkinter import *
from _ctypes import alignment
b = Tk()
Color = "Skyblue"                                                                                    #Fondo de las aplicaciones
Color1 = "Skyblue"                                                                                   #Fondo de los botones de la segunda pantalla
Color3 = "White"                                                                                    #Color de letra de los botones de la segunda pantalla
b.title("BANCA MOVIL ¡K-CHIMBOS!")                                                                  #TITULO DE LA VENTANA
b.config(bg= f"{Color}")                                                                            #COLOR DE FONDO
b.resizable(False,False)
b.geometry("600x500")                                                                               #DIMENSIONES
b.iconbitmap(r'D:\BIC01-X\fiis-intro192-lab\CODIGOAPPPP\k.ico')
for i in range(0,11):                                                                               #RELLENO de 0 a 9
    a = Label(b,text="",bg=f"{Color}",width=2)
    a.grid(column=0,row=i)
for i in range(11,16):                                                                              #RELLENO de 10 a más
    a = Label(b,text="",bg=f"{Color}",width=0)
    a.grid(column=0,row=i)
titulo = Label(b,text="BIENVENIDO",font=("Lucida Handwriting",50),bg=f"{Color}").place(x=50,y=20)   #TITULO DENTRO
l1=Label(b,text="INTRODUZCA EL NÚMERO DE LA TARJETA:",bg=f"{Color}",font=("Bahnschrift SemiBold",10)).place(x=20,y=150)
entry_n= StringVar()                                                                                # the text in  your entry
n=Entry(b,width=16,font=("arial",20), textvariable = entry_n)                                       #NUMERO DE TARJETA
def character_limit(entry_n):                                                                       #Definiendo la funcion para el maximo de caracteres del numero de cuenta
    if len(entry_n.get()) > 0:
        entry_n.set(entry_n.get()[:16])
entry_n.trace("w", lambda *args: character_limit(entry_n))
n.grid(column=1,row=8)                                                                              #Posición del número de la tarjeta
l2= Label(b,text="CONTRASEÑA:",bg=f"{Color}",font=("Bahnschrift SemiBold",10)).place(x=20,y=230)
entry_clave = StringVar()                                                                           # the text in  your entry
clave=Entry(b,width=4,font=("Arial",20),textvariable = entry_clave)                                 #CLAVE
def character_limit2(entry_clave):                                                                  #Definiendo la función para el máximo de caracteres de la clave
    if len(entry_clave.get()) > 0:
        entry_clave.set(entry_clave.get()[:4])
entry_clave.trace("w", lambda *args: character_limit2(entry_clave))
clave.grid(column=1,row=11, sticky= W)                                                              #Posición de la clave
def clicked():                                                                                      #SEGUNDA VENTANA
    b.withdraw()
    b2 = Tk()
    b2.config(bg= f"{Color}")
    b2.title("MENÚ PRINCIPAL")
    b2.resizable(False,False)
    b2.geometry("400x550")
    b2.iconbitmap(r'D:\BIC01-X\fiis-intro192-lab\CODIGOAPPPP\k.ico')
    t1= Label(b2,text="Seleccione una opción",font=("Lucida Handwriting",22),bg=f"{Color}").place(x=15,y=10)
    for i in range(0,20):
        a = Label(b2,text="",bg=f"{Color}",width=2)
        a.grid(column=0,row=i)
    def clicked1():
        b2.withdraw()
        b3 = Tk()
        b3.config(bg= f"{Color}")
        b3.title("ESTADO DE CUENTA")
        b3.resizable(False,False)
        b3.geometry("450x550")
        b3.iconbitmap(r'D:\BIC01-X\fiis-intro192-lab\CODIGOAPPPP\k.ico')
        for i in range(0,900):
            a = Label(b3,text="",bg=f"{Color}",width=2)
            a.grid(column=0,row=i)
        t2= Label(b3,text="El saldo actual de la tarjeta es: \n100.00 ",font=("Lucida Handwriting",15),bg=f"{Color}").place(x=5,y=10)
    def clicked2():
        b2.withdraw()
        b5 = Tk()
        b5.config(bg= f"{Color}")
        b5.title("TRANSFERENCIAS BANCARIAS")
        b5.resizable(False,False)
        b5.geometry("450x550")
        b5.iconbitmap(r'D:\BIC01-X\fiis-intro192-lab\CODIGOAPPPP\k.ico')
        for i in range(0,20):
            a = Label(b5,text="",bg=f"{Color}",width=2)
            a.grid(column=0,row=i)
        t5= Label(b5,text="Digite el numero de la tarjeta de la cuenta \na la que desea transferir:",font=("Lucida Handwriting",10),bg=f"{Color}").place(x=5,y=10)
        entry_clave2 = StringVar()                                                                           # the text in  your entry
        clave1=Entry(b5,width=16,font=("Arial",20),textvariable = entry_clave2)                                 #CLAVE
        def character_limit2(entry_clave2):                                                                  #Definiendo la función para el máximo de caracteres de la clave
            if len(entry_clave2.get()) > 0:
                entry_clave2.set(entry_clave2.get()[:16])
        entry_clave2.trace("w", lambda *args: character_limit2(entry_clave2))
        clave1.grid(column=1,row=3, sticky= W)                                                              #Posición de la clave
    def salir():
        b2.withdraw()
        b4 = Tk()
        b4.config(bg= f"{Color}")
        b4.title("SALIR")
        b4.resizable(False,False)
        b4.geometry("425x120")
        b4.iconbitmap(r'D:\BIC01-X\fiis-intro192-lab\CODIGOAPPPP\k.ico')
        for i in range(0,900):
            a = Label(b4,text="",bg=f"{Color}",width=2)
            a.grid(column=0,row=i)
        t4= Label(b4,text="Sesión terminada.\nPuede cerrar la ventana.",font=("Lucida Handwriting",20),bg=f"{Color}").place(x=16,y=10)
    r1 = Button(b2,text='1. CONSULTAR ESTADO DE CUENTA', bg= f"{Color1}",font=("Comic Sans MS",10),width=38,fg= f"{Color3}",command=clicked1)
    r2 = Button(b2,text='2. REALIZAR TRANSFERENCIAS BANCARIAS',bg= f"{Color1}",font=("Comic Sans MS",10),width=38,fg= f"{Color3}",command =clicked2)
    r3 = Button(b2,text='3. VER HISTORIAL DE LA CUENTA',bg= f"{Color1}",font=("Comic Sans MS",10),width=38,fg= f"{Color3}")
    r4 = Button(b2,text='4. REALIZAR PAGOS',bg= f"{Color1}",font=("Comic Sans MS",10),width=38,fg= f"{Color3}")
    r5 = Button(b2,text='5. VER MILLAS ACUMULADAS',bg= f"{Color1}",font=("Comic Sans MS",10),width=38,fg= f"{Color3}")
    Salir = Button(b2,text='SALIR',bg= f"{Color1}",font=("Comic Sans MS",10),width=38,fg= f"{Color3}",command=salir)                      #Botón salir
    r1.grid(column=7, row=4)
    r2.grid(column=7, row=7)
    r3.grid(column=7, row=10)
    r4.grid(column=7, row=13)
    r5.grid(column=7, row =16)
    Salir.grid(column=7, row=19)
    b2.mainloop()
verificar = Button(b,text="INGRESAR",font=("arial",10),width=20,height=5,command=clicked).place(x=210,y=330)
b.mainloop()
